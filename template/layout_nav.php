<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Plaza Comunitaria</a> <?php //TODO: make title a var in config|context ?>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="<?php echo ($this->getData('nav-active') == 'home' ? ' active' : '' ); ?>"><a href="/">Home</a></li>
        <li class="dropdown<?php echo ($this->getData('nav-active') == 'resources' ? ' active' : '' ); ?>">
			<a href="/page/resources/" class="dropdown-toggle" data-toggle="dropdown">Resources <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<?php include("$dir/template/page/resources_submenu.php"); ?>
			</ul>
		</li>
        <li class="<?php echo ($this->getData('nav-active') == 'calendar' ? ' active' : '' ); ?>"><a href="/page/calendar/">Calendar</a></li>
        <li class="<?php echo ($this->getData('nav-active') == 'friends' ? ' active' : '' ); ?>"><a href="/page/friends/">Friends</a></li>
        <li class="<?php echo ($this->getData('nav-active') == 'library' ? ' active' : '' ); ?>"><a href="/page/library/">Library</a></li>
        <li class="<?php echo ($this->getData('nav-active') == 'donations' ? ' active' : '' ); ?>"><a href="/page/donations/">Donations</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
