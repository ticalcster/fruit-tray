<?php
/*
 *  Start Session watching.
 */
session_start();


/*
 *  Global Variables
 */
$dir = dirname(__FILE__);

/*
 * Include PHP Global Classes
 */
require_once($dir.'/include/context.php');
require_once($dir.'/include/config.php');

$cfg = new Config();
$ctx = new Context();


//Database setup.
//TODO