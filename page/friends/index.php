<?php
require_once('../../_init.php');

// active menu item
$ctx->addData('nav-active','friends');

/*
 * Friends list
 * For now it's an array but kinda sloppy.
 * TODO: put in database with ability to edit.
 */
$friends = array(
	array(
		'image_url' => '/static/img/NHN.jpg',
		'title' => 'Neighbors Helping Neighbors Helping Neighbors',
		'description' => 'Neighbors-helping-Neighbors USA, Inc. (www.nhnusa.org) is a cost-free, grassroots, nationally recognized, and respected, successful Peer-led volunteer weekly job search support and networking group targeted to adults who are actively looking for work and interested in reinvigorating their job search. Membership is open to anyone in career transition, including unemployed or underemployed individuals, recent college graduates, and military veterans in the fields of business, non-profit, and education, as well as persons re-entering the job market, struggling small business owners and anyone looking for part-time or volunteer work.'
	),array(
		'image_url' => '',
		'title' => 'El Milagro',
		'description' => 'Since 1950, El Milagro has produced quality products which supports the diets of a full line of quality tortilla products for every application. "The Fame is in the Name, and the Quality is in the Product."'
	),array(
		'image_url' => '',
		'title' => 'Steepletown Neighborhood Services',
		'description' => 'Steepletown Neighborhood Services was initiated in 1994 by three neighboring Catholic Churches: St. James, St. Mary, and the Basilica of Saint Adalbert. They realized the value in working together to address the needs of persons living on the West Side of Grand Rapids. As a result, Steepletown was founded and incorporated in 1994. Its mission reflects the neighborliness of the West Side: "to promote neighbor helping neighbor live with dignity and hope." Steepletown accomplishes this mission through creative programs and services, and by managing a neighborhood center that provides space to other agencies working on the West Side. La Plaza Comunitaria is one of those organizations. It offers a variety of support services and educational programs, and manages a community center that provides space for several other human service agencies.'
	),array(
		'image_url' => '',
		'title' => 'Wellspring Lutheran Services',
		'description' => 'Guided by its core belief that every person has God-given potential, Wellspring Lutheran Services\' Foundation seeks to bring awareness to the rising needs of Michigan\'s children, families and seniors. With the support of other organizations, Wellspring Luthern offers services in the following areas: Senior Livng, Rehab Services, Home Care, Child and Family Services.'
	),array(
		'image_url' => '',
		'title' => 'Grand Rapids Community Foundation',
		'description' => 'Community Foundations build and strengthen communities by making it possible for a wide range of donors to participate in creating permanent (and often named) funds to meet present and future needs. Community foundations have become catalysts for improvement within urban centers and in rural settings through philanthropy that is visionary, diverse and inclusive. Grand Rapids Community Foundation\'s mission is to build and manage the community\'s permanent endowment and lead the community to strengthen the lives of its people.'
	),array(
		'image_url' => '',
		'title' => 'Grand Valley State University',
		'description' => 'Grand Valley State University is a public university in West Michigan providing a fully accredited undergraduate and graduate liberal education to more than 24,000 students. Grand Valley offers more than 200 areas of study, providing a wide selection of undergraduate majors and graduate programs.'
	),array(
		'image_url' => '',
		'title' => 'Dominican Center at Marywood',
		'description' => 'The mission of Dominican Center at Marywood is to foster the growth and the transformation of persons, communities and organizations through prayer, learning and collaboration in an inclusive, hospitable environment. A Spirituality and Conference Center, located on the peaceful 34-acre Marywood campus, Dominican Center welcomes individuals and groups.Retreatants seeking a contemplative environment find a home where they can take the next step and deepen their spiritual journey. In addition to retreats, individuals may participate in a variety of programs � focusing on one�s physical and spiritual needs.'
	),array(
		'image_url' => '',
		'title' => 'Consulate of Mexico in Detroit',
		'description' => 'The Detroit presence of Consulate of Mexico provides information about the services offered and legal proceeding that the Consulate can assist you with. With the purpose of serving the Mexican people�s needs and interests, the Consulate of Mexico in Detroit provides various services within the jurisdiction. Moreover, this Representation offers consular services of documentation to Mexicans and foreigners either for legal purposes or simply to help them meet the travel requirements needed to visit Mexico. In addition, the Consulate supports a diversity of social programs promoted by the Institute for Mexicans Abroad (IME) and furthermore, the Consulate is seriously engaged with the Mexican community�s security and wellbeing, as it provides legal counseling in issues that might attempt to endanger a Mexican national.'
	),array(
		'image_url' => '',
		'title' => 'West Michigan Latino Community Coalition',
		'description' => 'West Michigan Latino Community Coalition is a small group of Latino professionals. With the efforts of educators, and professionals, they colaborate to expose educational opportunity to the community. This is how La Plaza began.'
	)
);

// adds friend array to context
$ctx->addData('friends',$friends);

$ctx->render('page/friends');