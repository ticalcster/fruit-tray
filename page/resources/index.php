<?php
require_once('../../_init.php');

// active menu item
$ctx->addData('nav-active','resources');
// active submenu item
$ctx->addData('resources-submenu','basic');

$ctx->render('page/resources');