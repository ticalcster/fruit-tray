<div class="row">
	<div class="col-md-12">
		<h1>Donations</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<form class="form-horizontal" role="form">
			<div class="row">
				<div class="col-md-6">
					<h3>Contact Info</h3>
					<div class="form-group">
						<label for="inputFirstName" class="col-sm-4 control-label">First Name</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputFirstName" placeholder="First Name">
						</div>
					</div>
					<div class="form-group">
						<label for="inputLastName" class="col-sm-4 control-label">Last Name</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputLastName" placeholder="Last Name">
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-4 control-label">Email</label>
						<div class="col-sm-8">
							<input type="email" class="form-control" id="inputEmail" placeholder="Email">
						</div>
					</div>
				</div><!-- col-md-6 -->
				<div class="col-md-6">
					<h3>Donation Type</h3>
					<div class="form-group">
						<label for="inputTime" class="col-sm-4 control-label">Time</label>
						<div class="col-sm-8">
							<div class="checkbox">
								<label>
									<input type="checkbox">
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEquipment" class="col-sm-4 control-label">Equipment</label>
						<div class="col-sm-8">
							<div class="checkbox">
								<label>
									<input type="checkbox">
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputMoney" class="col-sm-4 control-label">Money</label>
						<div class="col-sm-8">                                      
							<input type="hidden" name="cmd" value="_s-xclick">
							<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHRwYJKoZIhvcNAQcEoIIHODCCBzQCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYA1JxDLvfg74EC80JRhxzxV40kmwtXfj8X7D+hkTcsgRATObo5f7PU/Wrkp+J04qYSlGFE6mPTHMZLA4vWgsynLjvQ6KC6GRcGUpw6aZt8gGp2oOLxgQEVHUv/6xN3FR6XtHMnVyjKAReUGncvkMXkWmIKmMYEP9fcX/LTOK6qWfzELMAkGBSsOAwIaBQAwgcQGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI4Wuk9O5lJGCAgaAZ7zMX/r02i+IWo74u3pZdfYx34hZHdHolBeerpBIIQatOMIHFhGXRTdzOBd6ZbPyO9C3su7qsGR532Ft+JNDpSxK9cH/duOKWABevNXIJDmqawzF6bIxWyyu2Wa8HRDiC7jvyruBJ7fmkmltKmjirO6cRvTqAG0aHvXaQx2Y6bxHslnd7WeZ6N9ZQlN2wybfv1HUiLVbRDr6nVw+AXMNZoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTQwMjI4MjAwMDMyWjAjBgkqhkiG9w0BCQQxFgQUxUNI13lyxNdu7PFBhU958lwpSqwwDQYJKoZIhvcNAQEBBQAEgYBSVeeAX1g9zeC9MzeoQlk/GGoh6owX8Faa1Arj6iJqRgB/9/Ss/JMTyKbLx//6TI6FG79Kq0cUXtluf4J+ZYOxlUJO/KPAJLvOowdejm6uOflunXe+zMTR3XjJASsKczk2sqwRz63RbNgj21NotxAGX3IfuhU75t3cXE+8In6atg==-----END PKCS7-----">
							<input type="image" class="" src="http://www.khalsaaid.org/images/donate-paypal.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
							<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
						</div>
		  			</div>
				</div><!-- col-md-6 -->
			</div><!-- row -->
			<div class="row">
				<div class="col-md-6">
					<h3>Description</h3>
					<textarea class="form-control" rows="6"></textarea>
				</div><!-- col-md-6 -->
				<div class="col-md-6">
					<h3>Additional Comments</h3>
					<textarea class="form-control" rows="6"></textarea>
				</div><!-- col-md-6 -->
			</div><!-- row -->
		</form>
	</div>
</div>

