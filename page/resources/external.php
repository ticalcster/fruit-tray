<?php
require_once('../../_init.php');

// active menu item
$ctx->addData('nav-active','resources');
// active submenu item
$ctx->addData('resources-submenu','external');

$ctx->render('page/resources');