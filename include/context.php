<?php
/**
 * The Context class holds all info about the current server request and response.
 */
class Context {
    protected $view = 'index'; //TODO: what page should be default?
    protected $store = array();
        
    public function __construct() {
        
    }
    public function getView() {
        // Returns the absolute path for the view name.
        global $dir;
        return "$dir/template/$this->view.php";
    }
    public function render($view) {
        // Renders (echos) the layout with view.
        global $dir;
        $this->view = $view;
        include_once("$dir/template/layout.php"); //TODO: move layout view to Config class.
    }

    public function redirect($url) {
        // Redirects page to url param.
        header("Location: $url");
        $_SESSION['redirect'] = $_SERVER['REQUEST_URI']; //TODO: don't use session for redirect
        exit; /* return with only location set */
    }
    public function redirectTo($url) {
        //TODO
    }
    
    /*
     *  Includes
     */
    public function includeCSS($css) {
        // Stack for including custom CSS files/style tags. 
        $this->store['page']['css'][] = $css;
    }
    public function includeJS($js) {
        // Stack for including custom JS files/script tags.
        $this->store['page']['js'][] = $js;
    }
    
    public function addData($index, $data) {
        // Add data to the context to be used in templates.
        $this->store['data'][$index] = $data;
    }
    public function getData($index) {
        // Return context data for use in templates.
        return $this->store['data'][$index];
    }
}