<div class="row">
	<div class="col-md-12">
		<h2>Friends of Plaza Comunitaria</h2>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
<?php foreach ( $this->getData('friends') as $friend ) { ?>
<div class="media">
  <a class="pull-left" href="#">
    <img class="media-object" src="<?php echo $friend['image_url'] ?>" alt="..." style="max-width:64px;">
  </a>
  <div class="media-body">
    <h4 class="media-heading"><?php echo $friend['title'] ?></h4>
    <?php echo $friend['description'] ?>
  </div>
</div>
<?php } ?>


	</div>
</div>
